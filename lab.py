#globalus kintamasis sekti dalumui
dalumas = 0

def set_globalus_to_zero():
	global dalumas
	dalumas = 0

#klase skirta skaityti duomenis is failo
class Skaitymo_Klase:

	__failas = ""
	__L = []
	__H = []

	def __init__(self,Failas):
		self.__failas = Failas

	#skaitymo is failo funkcija
	def Skaityti(self):
		with open(self.__failas,'r') as fd:
			T = fd.readline().strip()
			data = fd.readlines()
			for line in data:
				sk = line.split()
				self.__L.append(sk[0])
				self.__H.append(sk[1])

			return T,self.__L, self.__H

#klase skirta atlikti skaiciavimus su skaiciais
class Skaiciavimo_Klase(Skaitymo_Klase):

	def __init__(self,Failas):
		super().__init__(Failas)

	#funkcija skirta suzinoti ar skaicius yra pirminis
	def Pirminis(self,skaicius):
		isPrime = True

		if(skaicius == 1 or skaicius == 0):
			return False

		for i in range(2, int(skaicius / 2) + 1):
			if( skaicius % i == 0):
				return False

		if(isPrime):
			return True

	#funkcija skirta suzinoti su kokiu skaiciumi dalinasi be liekanos
	def Dalumas(self,skaicius):
		global dalumas
		set_globalus_to_zero()
		for j in range(1,skaicius + 1):
			if(skaicius % j == 0):
				dalumas = dalumas + 1

#main
x = Skaiciavimo_Klase("Duomenys.txt")
L = []
H = []
T,L,H = x.Skaityti()

#atliekami veiksmai
for z in range(0, int(T)):
	kiek = 0
	for j in range(int(L[z]), int(H[z]) + 1):
		x.Dalumas(j)
		if(x.Pirminis(dalumas)):
			kiek = kiek + 1
			print(j, end=" ")
	if (kiek == 0):
		print(-1, end=" ")
	print("")
